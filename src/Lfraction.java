import java.util.*;



/** This class represents fractions of form n/d where n and d are long integer 

 * numbers. Basic operations and arithmetics for fractions are provided.

 */

public class Lfraction implements Comparable<Lfraction> {



   /** Main method. Different tests. 

 * @throws CloneNotSupportedException */

   public static void main (String[] param) throws CloneNotSupportedException {  

	   //Lfraction x = new Lfraction(Integer.parseInt(param[0]),Integer.parseInt(param[1]));

	 //  Lfraction x = new Lfraction(3,5);

	  // Lfraction y = new Lfraction(5,8);

	  // System.out.println(x.getNumerator());

	 //  System.out.println(x.getDenominator());

	 //  System.out.println(x.equals(y));

	 //  System.out.println(x.hashCode());

	 //  System.out.println(y.hashCode());

	 //  System.out.println(x.plus(y));

	  // System.out.println(x.times(y));

	  // System.out.println(x.inverse());

	 //  System.out.println(x.opposite());

	 //  System.out.println(x.compareTo(y));

	//   Lfraction z = (Lfraction) x.clone();

	  // System.out.println(z.toString());

	 // System.out.println(z.integerPart());

	//   System.out.println(z.fractionPart());

	 //  System.out.println(z.toDouble());

	 //  Lfraction aa = toLfraction(1.825,8);

	 //  System.out.println(aa.toString());

	//   Lfraction zz = Lfraction.valueOf("25/125");

	 //  System.out.println(zz.toString());   

   }

   

   private long mynumerator;

   private long mydenominator;

   /** Constructor.

    * @param a numerator

    * @param b denominator > 0

    */

   public Lfraction (long a, long b) {
	   if (b > 0) {
			mynumerator = a;
			mydenominator = b;
		} else if (b < 0) {
			mynumerator = -a;
			mydenominator = -b;
		} else
			throw new ArithmeticException(" illegal denominator zero ");
	}


   /** Public method to access the numerator field. 

    * @return numerator

    */

   public long getNumerator() {

      return mynumerator; 

   }

   



   /** Public method to access the denominator field. 

    * @return denominator

    */

   public long getDenominator() { 

      return mydenominator; 

   }



   /** Conversion to string.

    * @return string representation of the fraction

    */

   @Override

   public String toString() {

     // return (String.valueOf(getNumerator()) + "/" + String.valueOf(getDenominator())); 

	   return Long.toString(mynumerator)+"/"+Long.toString(mydenominator);

   }



   /** Equality test.

    * @param m second fraction

    * @return true if fractions this and m are equal

    */

   @Override

   public boolean equals (Object m) {

	   return (compareTo ((Lfraction)m) == 0); 
   }



   /** Hashcode has to be equal for equal fractions.

    * @return hashcode

    */

   @Override

   public int hashCode() {

	   return Objects.hash(mydenominator, mynumerator); 

   }



   /** Sum of fractions.

    * @param m second addend

    * @return this+m

    */


 public Lfraction plus (Lfraction m) {

	 return new Lfraction((mynumerator*m.mydenominator)+(m.mynumerator*mydenominator),(mydenominator*m.mydenominator));



   }



   /** Multiplication of fractions.

    * @param m second factor

    * @return this*m

    */

 	// TODO!!!

    public Lfraction times (Lfraction m) {

      return new Lfraction(mynumerator * m.mynumerator, mydenominator * m.mydenominator).reduce();



   }



   /** Inverse of the fraction. n/d becomes d/n.

    * @return inverse of this fraction: 1/this

    */

  public Lfraction inverse() {

	  if(mynumerator > 0){

	         return new Lfraction(mydenominator, mynumerator);

	      }else if(mynumerator == 0){

	         throw  new RuntimeException("denominator can't be 0");

	      }

	      else{

	         return new Lfraction((-1)*mydenominator, (-1)*mynumerator);

	      } 

   }



   /** Opposite of the fraction. n/d becomes -n/d.

    * @return opposite of this fraction: -this

    */


  public Lfraction opposite() {

	   return new Lfraction(mynumerator*(-1), mydenominator); 

   }



   /** Difference of fractions.

    * @param m subtrahend

    * @return this-m

    */


   public Lfraction minus (Lfraction m) {

	      if(mydenominator == m.mydenominator){

	          return new Lfraction((mynumerator-m.mynumerator), mydenominator);

	       } else {

	          return new Lfraction(((mynumerator*m.mydenominator)- (m.mynumerator*mydenominator)), (mydenominator*m.mydenominator));

	       }

	    }



   /** Quotient of fractions.

    * @param m divisor

    * @return this/m

    */


   public Lfraction divideBy (Lfraction m) {

	   if (m.mynumerator==0){

		   throw new ArithmeticException("denominator can't be 0");

	   }

	   Lfraction div = m.inverse();

      return times(div);

   }



   /** Comparision of fractions.

    * @param m second fraction

    * @return -1 if this < m; 0 if this==m; 1 if this > m

    */

   @Override


    public int compareTo (Lfraction m) {

      if (mynumerator* m.mydenominator < m.mynumerator*mydenominator) return -1;

      else if (mynumerator* m.mydenominator == m.mynumerator*mydenominator) return -0;

      else return 1;

   }



   /** Clone of the fraction.

    * @return new fraction equal to this

    */

   @Override


   public Object clone() throws CloneNotSupportedException {

	   return new Lfraction(mynumerator, mydenominator);

   }



   /** Integer part of the (improper) fraction. 

    * @return integer part of this fraction

    */

   public long integerPart() {

      return mynumerator / mydenominator; 

   }



   /** Extract fraction part of the (improper) fraction

    * (a proper fraction without the integer part).

    * @return fraction part of this fraction

    */

   public Lfraction fractionPart() { 

	   return new Lfraction (mynumerator % mydenominator, mydenominator); // 

   }



   /** Approximate value of the fraction.

    * @return numeric value of this fraction

    */


  public double toDouble() {

	  return (double)mynumerator/mydenominator; 

   }



   /** Double value f presented as a fraction with denominator d > 0.

    * @param f real number

    * @param d positive denominator for the result

    * @return f as an approximate fraction of form n/d

    */


   public static Lfraction toLfraction (double f, long d) {

	   return new Lfraction((int) Math.round(f*d), d);

   }



   /** Conversion from string to the fraction. Accepts strings of form

    * that is defined by the toString method.

    * @param s string form (as produced by toString) of the fraction

    * @return fraction represented by s

    */

   public static Lfraction valueOf (String s) {

	   String[] parts = s.split("/");

      return new Lfraction((long)Integer.valueOf(parts[0]), (long)Integer.valueOf(parts[1])); 

   }
   

   /** Reduce this fraction (and make denominator > 0).
    * @return reduced fraction
    */
private Lfraction reduce() {
	Lfraction f = null;
	try {
		f = (Lfraction) clone();
	} catch (CloneNotSupportedException e) {
	}
	;
	if (mydenominator == 0)
		throw new ArithmeticException(" illegal denominator zero ");
	if (mydenominator < 0) {
		f.mynumerator = -mynumerator;
		f.mydenominator = -mydenominator;
	}
	if (mynumerator == 0)
		f.mydenominator = 1;
	else {
		long n = gcd(mynumerator, mydenominator);
		f.mynumerator = mynumerator / n;
		f.mydenominator = mydenominator / n;
	}
	return f;
}

/** Greatest common divisor of two given integers.
 * @param a first integer
 * @param b second integer
 * @return GCD(a,b)
 */
private static long gcd(long a, long b) {
	long m = Math.max(Math.abs(a), Math.abs(b));
	if (m == 0)
		throw new ArithmeticException(" zero in gcd ");
	long n = Math.min(Math.abs(a), Math.abs(b));
	while (n > 0) {
		a = m % n;
		m = n;
		n = a;
	}
	return m;
}
}

//https://www2.hawaii.edu/~walbritt/ics211/examples/Fraction.java
//http://stackoverflow.com/questions/4201860/how-to-find-gcd-lcm-on-a-set-of-numbers
